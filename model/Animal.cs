﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task9.model
{
    class Animal
    {
        #region Attributes

        private string species;
        private string favoriteFood;
        private bool isLandAnimal;

        #endregion

        #region Constructors
        // There are three constructors; one empty, one with two arguments and one with three arguments.

        public Animal() { }

        public Animal(string species, string favoriteFood)
        {
            this.species = species;
            this.favoriteFood = favoriteFood;

        }

        public Animal(string species, string favoriteFood, bool isLandAnimal)
        {
            this.species = species;
            this.favoriteFood = favoriteFood;
            this.isLandAnimal = isLandAnimal;
        }

       

        #endregion

        #region Behaviour
        // I have created three methods, one to get the animal's favorite food, one to get the
        // animal's species and one to find out if it is a landbased animal or an underwaterbased
        // animal.

        public void eat()
        {
            if (favoriteFood != null)
            {
                Console.WriteLine( $"My favorite is {favoriteFood}");
            }
            else
            {
                Console.WriteLine( "This animal does not have a favorite food.");
            }
        }

        public void animalSpecies()
        {
            if (species != null)
            {
                Console.WriteLine($"This animal is a {species}.");
            }
            else
            {
                Console.WriteLine("This animal is of unknown species.");
            }
        }

        public void landAnimal()
        {
            if (isLandAnimal == true)
            {
                Console.WriteLine("This animal lives on land.");
            }
            if (isLandAnimal == false)
            {
                Console.WriteLine("This animal lives in water.");
            }
            else
            {
                Console.WriteLine("We have no information on this animal.");
            }
        }

        #endregion

        #region Overrides
        // Here I override the ToString() to get the correct outcome when displaying the list of
        // animals to the user.

        override
        public string ToString()
        {
            return $"{species}\nEats: {favoriteFood}\n";
        }

        #endregion

        #region Access Modifiers

        // In this region the access modifiers are, more as a test for myself than something
        // useful in this particular task.

        public string GetSpecies()
        {
            return species;
        }

        public void SetSpecies(string species)
        {
            this.species = species;
        }

        #endregion


    }
}
