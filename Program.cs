﻿using System;
using Task9.model;
using System.Collections.Generic;

namespace Task9
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Creating the animals
            // Here the animals are created with different constructors.

            Animal Rabbit = new Animal("Rabbit", "Carrot", true);
            Animal Dog = new Animal("Dog", "Bones");
            Animal Dolphin = new Animal("Dolphin", "Sardins", false);
            Animal Horse = new Animal("Horse", "Hay", true);

            #endregion

            #region Trying out some methods
            // Some of the methods are tested.

            Horse.eat();
            Console.WriteLine();
            Dolphin.landAnimal();
            Console.WriteLine();

            #endregion

            #region Making a list of animals
            // A list of all the animals are created and displayed for the user with the first
            // two properties which are common for all.

            List<Animal> animalList = new List<Animal>();

            animalList.Add(Rabbit);
            animalList.Add(Dog);
            animalList.Add(Dolphin);
            animalList.Add(Horse);


            foreach (Animal animal in animalList)
            {
                Console.WriteLine(animal);
            }

            #endregion

        }
    }
}
